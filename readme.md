#面试常见问题收集（持续更新）
##代码编写类
1. ArrayRandomTest 随机打乱一个指定数组
2. FatherAndSonTest 子类、父类 的执行顺序
3. PingPongThreadTest 调用Thread run方法
4. ThreadCommunicationTest 线程间通信1
5. ThreadExecuteOrderTest 线程间通信2

##问答类
1. Spring中Model和ModelAndView的区别？
  - Model：只用作传输数据
  - ModelAndView：即传输数据也指定返回视图
  - Model：Controller中每个请求方法自带
  - ModelAndView：需要自行创建

2. Spring是如何绑定表单对象的？如果是Ajax提交呢？
  - 表单提交：<form>标签使用 commandName，controller方法使用 @ModelAttribute ，原理是使用 DataBinder来实现
  - ajax提交：
    ```javascript
    $.ajax({
        type:'post',
        datatype:'text',
        url:'xx/xx/save',
        //注意：serializeArray()方法将页面表单序列化成一个JSON结构的对象。注意不是JSON字符串。
        data:$('formId').serializeArray(),
        contentType:'application/x-www-form-urlencoded',
        success:function(){
           
        }
    });
    ```

3. 数组与链表的区别？
  - 数组：
    数组是将元素在内存中连续存放，由于每个元素占用内存相同，可以通过下标迅速访问数组中任何元素。但是如果要在数组中增加一个元素，需要移动大量元素，在内存中空出一个元素的空间，然后将要增加的元素放在其中。同样的道理，如果想删除一个元素，同样需要移动大量元素去填掉被移动的元素。如果应用需要快速访问数据，很少或不插入和删除元素，就应该用数组。
  - 链表：
    链表恰好相反，链表中的元素在内存中不是顺序存储的，而是通过存在元素中的指针联系到一起。比如：上一个元素有个指针指到下一个元素，以此类推，直到最后一个元素。如果要访问链表中一个元素，需要从第一个元素开始，一直找到需要的元素位置。但是增加和删除一个元素对于链表数据结构就非常简单了，只要修改元素中的指针就可以了。如果应用需要经常插入和删除元素你就需要用链表数据结构了。

4. Spring中的Controller默认是单例的还是多例的？如果是单例的，那可以指定多例吗，如何开启？如果是单例的，那在高并发访问下怎么保证响应性能呢？
  - Spring中的Controller默认是单例（singleton）的
  - 可以开启多例，使用 @Scope("prototype") 注解
  - Spring单例模式下使用ThreadLocal来切换不同线程之间的参数，虽然是在同一个实例Controller下，但是参数都是互相隔离的，则保证线程安全，同时又因为是单例的，创建和销毁bean的操作大大减少，减少了对内存的消耗，反而提升了性能
  - 总的来说就是，单利模式因为大大节省了实例的创建和销毁，有利于提高性能，而ThreadLocal用来保证线程安全性
  - 最佳实践：不要在Controller中定义非静态变量的成员变量，万不得已时通过使用注解@Scope("prototype")设置为多例模式
  
5. 分布式事务的解决办法？
  - 1、结合MQ消息中间件实现的可靠消息最终一致性 
  - 2、TCC补偿性事务解决方案 
  - 3、最大努力通知型方案 
  - 第一种方案：可靠消息最终一致性，需要业务系统结合MQ消息中间件实现，在实现过程中需要保证消息的成功发送及成功消费。即需要通过业务系统控制MQ的消息状态 
  - 第二种方案：TCC补偿性，分为三个阶段TRYING-CONFIRMING-CANCELING。每个阶段做不同的处理。 TRYING阶段主要是对业务系统进行检测及资源预留 CONFIRMING阶段是做业务提交，通过TRYING阶段执行成功后，再执行该阶段。默认如果TRYING阶段执行成功，CONFIRMING就一定能成功。 CANCELING阶段是回对业务做回滚，在TRYING阶段中，如果存在分支事务TRYING失败，则需要调用CANCELING将已预留的资源进行释放。 
  - 第三种方案：最大努力通知xing型，这种方案主要用在与第三方系统通讯时，比如：调用微信或支付宝支付后的支付结果通知。这种方案也是结合MQ进行实现，例如：通过MQ发送http请求，设置最大通知次数。达到通知次数后即不再通知。