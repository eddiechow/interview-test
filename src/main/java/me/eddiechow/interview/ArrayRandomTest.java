package me.eddiechow.interview;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @description: 随机打乱一个数组
 * 1、利用 Collections.shuffle(arr) 工具类
 * 2、利用 Random().nextInt(arr.length) 做交换
 * @author: zhoudi
 * @date: 2017/6/12 17:26
 */
public class ArrayRandomTest {
    public static void printArr(String[] arr){
        if (arr == null){
            System.out.println("arr is null");
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+", ");
        }
    }

    public static void random1(){
        String[] arr = {"a","b","c","d","e","f"};
        List<String> list = Arrays.asList(arr);
        Collections.shuffle(list);
        printArr(arr);
    }

    public static void random2(){
        String[] arr = {"a","b","c","d","e","f"};
        String temp = "";
        for (int i = 0; i < arr.length; i++) {
            int random = new Random().nextInt(arr.length);
            temp = arr[i];
            arr[i] = arr[random];
            arr[random] = temp;
        }
        printArr(arr);
    }

    public static void main(String[] args) {
        random1();
        random2();
    }
}
