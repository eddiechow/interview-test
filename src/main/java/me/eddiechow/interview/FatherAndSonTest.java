package me.eddiechow.interview;

/**
 * @description: 子类、父类 构造方法、静态代码块、非静态代码块 执行顺序
 * @author: zhoudi
 * @date: 2017/6/12 17:45
 */
public class FatherAndSonTest {
    static class Father{
        private String name;
        static {
            System.out.println("父类静态代码块");
        }
        {
            System.out.println("父类非静态代码块");
        }
        public Father(){
            System.out.println("父类默认无参构造方法");
        }
        public Father(String name){
            this.name = name;
            System.out.println("父类带参数构造方法");
        }
    }
    static class Son extends Father{
        private String name;
        static {
            System.out.println("子类静态代码块");
        }
        {
            System.out.println("子类非静态代码块");
        }
        public Son(){
            System.out.println("子类默认无参构造方法");
        }
        public Son(String name){
            System.out.println("子类带参数构造方法");
        }
    }

    public static void main(String[] args) {
        new Son("eddie");
    }
}
