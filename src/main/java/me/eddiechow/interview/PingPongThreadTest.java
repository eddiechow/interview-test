package me.eddiechow.interview;

/**
 * @description: 关于直接调用 Thread run方法的测试
 * @author: zhoudi
 * @date: 2017/6/12 17:43
 */
public class PingPongThreadTest {
    public static void main(String[] args) {
        Thread thread = new Thread(){
            @Override
            public void run() {
                pong();
            }
        };
        thread.run();
        System.out.print("ping");
        //打印结果，pongping
        //直接调用run方法不会开启新的线程
    }
    static void pong(){
        System.out.print("pong");
    }
}
